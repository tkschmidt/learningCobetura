
[![coverage report](https://gitlab.com/tkschmidt/learningCobetura/badges/master/coverage.svg)](https://gitlab.com/tkschmidt/learningCobetura/-/commits/master)

```
mkdir build && cd build
make .. -DCMAKE_BUILD_TYPE=Debug
make coverage


## TODO
Figure out how to get the total number from gcovr
```
gcovr -e tests/catch.hpp # is a start
available reports at https://tkschmidt.gitlab.io/learningCobetura
```

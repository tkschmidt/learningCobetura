#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "TEST_DO_MATH_LIB.hpp"

TEST_CASE( "factorials are computed", "[factorial]" ) {
	REQUIRE( Factorial(2) == 2 );
	REQUIRE( Factorial(3) == 6 );
	REQUIRE( Factorial(10) == 3628800 );
	REQUIRE( Factorial(1) == 1 );
}
TEST_CASE( "check positive", "[factorial]" ) {
	REQUIRE( isPositive(2) == true );
}
